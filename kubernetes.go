package main

import (
	"errors"
	"fmt"

	v1 "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

func NewClientSet() (kubernetes.Interface, error) {
	loadingRules := clientcmd.NewDefaultClientConfigLoadingRules()
	kubeConfig := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(loadingRules, &clientcmd.ConfigOverrides{})
	config, err := kubeConfig.ClientConfig()
	if err != nil {
		return nil, err
	}
	return kubernetes.NewForConfig(config)
}

func ValidateProbeAsReadiness(probe *v1.Probe) error {
	if probe == nil {
		return errors.New("empty configuration")
	}
	if probe.HTTPGet != nil {
		return nil
	}
	delay := probe.InitialDelaySeconds
	if probe.Exec != nil {
		return fmt.Errorf("empty readiness probe http configuration, but exec found [initial_delay=%ds]", delay)
	}
	if probe.TCPSocket != nil {
		return fmt.Errorf("empty readiness probe http configuration, but tcp found [initial_delay=%ds]", delay)
	}
	return errors.New("empty readiness probe http configuration")
}
