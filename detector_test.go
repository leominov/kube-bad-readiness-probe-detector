package main

import (
	"testing"

	"github.com/stretchr/testify/assert"

	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/fake"
)

func TestDetector_Run(t *testing.T) {
	k8sClient := fake.NewSimpleClientset(
		&v1.Namespace{
			ObjectMeta: metav1.ObjectMeta{
				Name: "app-namespace",
			},
		},
		&v1.Namespace{
			ObjectMeta: metav1.ObjectMeta{
				Name: "foobar",
			},
		},
		&v1.Namespace{
			ObjectMeta: metav1.ObjectMeta{
				Name: "app-foobar",
			},
		},
		&appsv1.Deployment{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "app1",
				Namespace: "app-namespace",
			},
			Spec: appsv1.DeploymentSpec{
				Template: v1.PodTemplateSpec{
					Spec: v1.PodSpec{
						Containers: []v1.Container{
							{
								Name: "app1",
								ReadinessProbe: &v1.Probe{
									Handler: v1.Handler{
										Exec: &v1.ExecAction{
											Command: []string{"foobar"},
										},
									},
								},
							},
							{
								Name: "app2",
								ReadinessProbe: &v1.Probe{
									Handler: v1.Handler{
										HTTPGet: &v1.HTTPGetAction{
											Path: "/-/ready",
										},
									},
								},
							},
						},
					},
				},
			},
		},
		&appsv1.Deployment{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "app2",
				Namespace: "foobar",
			},
			Spec: appsv1.DeploymentSpec{
				Template: v1.PodTemplateSpec{
					Spec: v1.PodSpec{
						Containers: []v1.Container{
							{
								Name: "app1",
								ReadinessProbe: &v1.Probe{
									Handler: v1.Handler{
										Exec: &v1.ExecAction{
											Command: []string{"foobar"},
										},
									},
								},
							},
							{
								Name: "app2",
								ReadinessProbe: &v1.Probe{
									Handler: v1.Handler{
										HTTPGet: &v1.HTTPGetAction{
											Path: "/-/ready",
										},
									},
								},
							},
						},
					},
				},
			},
		},
	)
	d := &Detector{
		namespaceFilter:         "app",
		deploymentLabelSelector: "name!=tiller",
		k8sClient:               k8sClient,
	}
	report, err := d.Run()
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, report.TotalItems, len(report.Items))
	if assert.Equal(t, 1, report.TotalItems) {
		item := report.Items[0]
		assert.Equal(t, "app-namespace", item.Namespace)
		assert.Equal(t, "app1", item.Container)
		assert.Equal(t, "app1", item.Deployment)
		assert.NotEmpty(t, item.Error)
	}
}
