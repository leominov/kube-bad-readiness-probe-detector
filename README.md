# kube-bad-readiness-probe-detector

Поиск шаблонов подов с некорректной конфигурацией Readiness Probe контейнера. Критерии описаны в файле `kubernetes.go`.

## Запуск

```shell
Usage of ./kube-bad-readiness-probe-detector:
  -deployment-label-selector string
    	Label selector for deployments (default "name!=tiller")
  -log-level string
    	Level of logging (default "info")
  -namespace-filter string
    	Filter for namespaces (default "app")
  -report-format string
    	Report format (csv, json, yaml) (default "csv")
```

## Пример отчета

```json
{
  "items": [
    {
      "container": "frontend",
      "deployment": "web-widget-texts",
      "error": "empty readiness probe http configuration, but tcp found",
      "namespace": "app-web-widget-texts",
      "readiness_probe": "\u0026Probe{Handler:Handler{Exec:nil,HTTPGet:nil,TCPSocket:\u0026TCPSocketAction{Port:{0 7766 },Host:,},},InitialDelaySeconds:5,TimeoutSeconds:1,PeriodSeconds:10,SuccessThreshold:1,FailureThreshold:3,}"
    }
  ],
  "scan_date": "2021-07-12T14:37:29.140806+05:00",
  "total_items": 1
}
```
