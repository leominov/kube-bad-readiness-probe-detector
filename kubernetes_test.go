package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	v1 "k8s.io/api/core/v1"
)

func TestValidateProbeAsReadiness(t *testing.T) {
	tests := []struct {
		probe *v1.Probe
		valid bool
	}{
		{
			probe: nil,
			valid: false,
		},
		{
			probe: &v1.Probe{},
			valid: false,
		},
		{
			probe: &v1.Probe{
				Handler: v1.Handler{
					Exec: &v1.ExecAction{
						Command: []string{"foobar"},
					},
				},
			},
			valid: false,
		},
		{
			probe: &v1.Probe{
				Handler: v1.Handler{
					TCPSocket: &v1.TCPSocketAction{
						Host: "localhost",
					},
				},
			},
			valid: false,
		},
		{
			probe: &v1.Probe{
				Handler: v1.Handler{
					HTTPGet: &v1.HTTPGetAction{
						Path: "/-/ready",
					},
				},
			},
			valid: true,
		},
	}
	for _, test := range tests {
		assert.Equal(t, test.valid, ValidateProbeAsReadiness(test.probe) == nil)
	}
}
