package main

import (
	"flag"
	"fmt"

	"github.com/sirupsen/logrus"
)

var (
	deploymentLabelSelector = flag.String("deployment-label-selector", "name!=tiller", "Label selector for deployments")
	logLevel                = flag.String("log-level", "info", "Level of logging")
	namespaceFilter         = flag.String("namespace-filter", "app", "Filter for namespaces")
	reportFormat            = flag.String("report-format", "csv", "Report format (csv, json, yaml)")
)

func main() {
	flag.Parse()

	if level, err := logrus.ParseLevel(*logLevel); err == nil {
		logrus.SetLevel(level)
	}

	k8sClient, err := NewClientSet()
	if err != nil {
		logrus.Fatal(err)
	}

	detector := &Detector{
		deploymentLabelSelector: *deploymentLabelSelector,
		k8sClient:               k8sClient,
		namespaceFilter:         *namespaceFilter,
	}

	report, err := detector.Run()
	if err != nil {
		logrus.Error(err)
	}

	fmt.Println(report.String(*reportFormat))
}
