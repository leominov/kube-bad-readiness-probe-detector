package main

import (
	"encoding/json"
	"time"

	"github.com/gocarina/gocsv"
	"gopkg.in/yaml.v3"
)

type Report struct {
	Items      []*ReportItem `json:"items"`
	ScanDate   time.Time     `json:"scan_date"`
	TotalItems int           `json:"total_items"`
}

type ReportItem struct {
	Container      string `json:"container"`
	Deployment     string `json:"deployment"`
	Error          string `json:"error"`
	Namespace      string `json:"namespace"`
	ReadinessProbe string `json:"readiness_probe" csv:"-"`
}

func (r *Report) String(format string) string {
	var b []byte
	switch format {
	case "yaml":
		b, _ = yaml.Marshal(r)
	case "json":
		b, _ = json.MarshalIndent(r, "", "  ")
	default: // csv
		b, _ = gocsv.MarshalBytes(r.Items)
	}
	return string(b)
}
